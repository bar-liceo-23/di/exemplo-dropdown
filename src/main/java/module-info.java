module net.liceolapaz.bar.di.examples.exemplodropdown {
    requires javafx.controls;
    requires javafx.fxml;
            
                            
    opens net.liceolapaz.bar.di.examples.exemplodropdown to javafx.fxml;
    exports net.liceolapaz.bar.di.examples.exemplodropdown;
}