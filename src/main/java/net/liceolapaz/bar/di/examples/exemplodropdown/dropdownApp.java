package net.liceolapaz.bar.di.examples.exemplodropdown;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class dropdownApp extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(dropdownApp.class.getResource("dropdown-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 300);
        stage.setTitle("Dropdown Exemplo");
        stage.setScene(scene);
        stage.show();
        // obtemos o controller
        DropdownController dController = fxmlLoader.getController();
        // enchemos o dropdown
        dController.fillProfessionMapIfIsEmpty();
    }

    public static void main(String[] args) {
        launch();
    }
}