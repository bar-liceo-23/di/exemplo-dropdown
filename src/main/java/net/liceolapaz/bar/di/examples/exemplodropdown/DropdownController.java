package net.liceolapaz.bar.di.examples.exemplodropdown;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.HashMap;

public class DropdownController {
    @FXML
    private Text textHello;
    @FXML
    private ChoiceBox choiceProf;
    @FXML
    private TextField tfName;

    HashMap<String,Color> professionMap = new HashMap<String,Color>();

    protected void fillProfessionMapIfIsEmpty(){
        /*
        Con esta función enchemos o hashmap e o dropdown se están vacios
         */
        if (professionMap.isEmpty()) {
            professionMap.put("", Color.BLACK);
            professionMap.put("Developer", Color.RED);
            professionMap.put("Architect", Color.AQUA);
            professionMap.put("SysOp", Color.GREEN);
            professionMap.put("DevOp", Color.GOLD);
            professionMap.put("Professor", Color.ORANGE);
            professionMap.put("Salesman", Color.BROWN);
            professionMap.put("Other", Color.DARKGREY);
            choiceProf.getItems().addAll(this.professionMap.keySet());
        }
    }
    @FXML
    protected void onButtonHello() {
        /*
        Cando pulsamos o botón de saudar escribimos o saúdo
        Tamén llle damos a cor correspondente según a profisión escollida.
         */

        String name = tfName.getText(); //Collemos o contido do TextField
        if (name.isEmpty()){
            //Se é vacio vamos poñer na mensaxe de saúdo que escolla un nome
            name += "chorvo, escribe o teu nome!";
        }
        //así poñemos o texto que queremos no texto de saúdo
        textHello.setText("Ola " + name);

        //así poñemos unha fonte distinta á por defecto
        textHello.setFont(new Font("Courier New",30));

        String profesion = null;
        try {
            /*
            Usar try-catch para revisar os valores do choice é moi sano
            para evitar os valores null cando non se escolle ningún valor nel
             */
            profesion = choiceProf.getValue().toString();
        } catch (Exception e){
            // Se non se puido coller o valor, poñemos string vacío
            profesion = "";
        }
        if (professionMap.containsKey(profesion)){
            textHello.setFill(professionMap.get(profesion));
        } else {
            textHello.setFill(Color.WHITE);
        }
    }

    @FXML
    protected void onButtonReload() {
        /*
        Nesta función restablecemos a interface
         */
        textHello.setText(""); //texto de saudo vacío
        choiceProf.setValue(""); //dropdown vacío
        tfName.setText(""); //textfield vacío

    }
}